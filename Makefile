TEXS = $(wildcard 20*.tex)
DVIS = $(TEXS:.tex=.dvi)
POSTSCRIPTS = $(DVIS:.dvi=.ps)

.PHONY: all clean none
none: ;

all: $(POSTSCRIPTS)

clean:
	rm -f *.dvi $(POSTSCRIPTS) *.aux *.log

%.dvi: %.tex
	latex $<

%.ps: %.dvi
	dvips -t letter -o $@ $<

%.pdf: %.ps
	ps2pdf -sPAPERSIZE=letter $<
